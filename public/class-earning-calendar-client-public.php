<?php

namespace Earning_Calendar\Client;

/**
 * The public-facing functionality of the plugin.
 *
 * @since      1.0.0
 *
 * @package    Earning_Calendar_Client
 * @subpackage Earning_Calendar_Client/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Earning_Calendar_Client
 * @subpackage Earning_Calendar_Client/public
 * @author     erikza@wizardsdev.com
 */
class Earning_Calendar_Client_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $earning_calendar_client    The ID of this plugin.
	 */
	private $earning_calendar_client;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @param      string    $earning_calendar_client       The name of the plugin.
	 * @param string $version    The version of this plugin.
	 *@since    1.0.0
	 */
	public function __construct(string $earning_calendar_client, string $version ) {

		$this->earning_calendar_client = $earning_calendar_client;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles_and_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Earning_Calendar_Client_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Earning_Calendar_Client_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->earning_calendar_client, EARNING_CALENDAR_PLUGIN_URL . 'client/public/assets/css/index.css', array(), $this->version, 'all' );
        wp_enqueue_script( $this->earning_calendar_client, EARNING_CALENDAR_PLUGIN_URL . 'client/public/assets/js/index.js', array(), $this->version, true );

	}

}
