<?php
namespace Earning_Calendar;

use Earning_Calendar\Includes\Earning_Calendar_Client;
use Earning_Calendar\Includes\Earning_Calendar_Client_Activator;
use Earning_Calendar\Includes\Earning_Calendar_Client_Deactivator;

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @since             1.0.0
 * @package           Earning_Calendar_Client
 *
 * @wordpress-plugin
 * Plugin Name:       Earning calendar client
 * Plugin URI:        $pluginUrl
 * Description:       Клиентская часть для плагина Earning calendar
 * Version:           1.0.0
 * Author:            erikza@wizardsdev.com
 * Author URI:        $url
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       earning-calendar-client
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Update it as you release new versions.
 */

define('EARNING_CALENDAR_PLUGIN_URL', plugin_dir_url( __FILE__ ));
require_once __DIR__ . DIRECTORY_SEPARATOR . 'config.php';
require_once EARNING_CALENDAR_PLUGIN_PATH . 'class-autoloader.php';


/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-earning-calendar-client-activator.php
 */

function activate_earning_calendar_client() {
	Earning_Calendar_Client_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-earning-calendar-client-deactivator.php
 */
function deactivate_earning_calendar_client() {
	Earning_Calendar_Client_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'Earning_Calendar\activate_earning_calendar_client' );
register_deactivation_hook( __FILE__, 'Earning_Calendar\deactivate_earning_calendar_client' );

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_earning_calendar_client() {

	$plugin = new Earning_Calendar_Client();
	$plugin->run();

}
run_earning_calendar_client();
