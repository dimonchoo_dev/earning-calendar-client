<?php

const EARNING_CALENDAR_SERVER_URL = 'https://ecs.devshell.site';
const EARNING_CALENDAR_CONFIG_URL = 'https://ecs.devshell.site/api/';
const EARNING_CALENDAR_ASSETS_URL = 'https://ecs.devshell.site/api/assets';
const EARNING_CALENDAR_UPDATE_URL = 'https://ecs.devshell.site/api/clientupdate';
const EARNING_CALENDAR_ERROR_LOG_URL = 'https://ecs.devxshell.site/api/sendError';
const EARNING_CALENDAR_CLIENT_VERSION = '1.0.0';
const EARNING_CALENDAR_PLUGIN_PATH = __DIR__ . DIRECTORY_SEPARATOR;
const EARNING_CALENDAR_CONFIGS_PATH = EARNING_CALENDAR_PLUGIN_PATH . 'app/configs/';
const EARNING_CALENDAR_TEMPLATES_PATH = EARNING_CALENDAR_PLUGIN_PATH . 'public/partials/tables/';
const EARNING_CALENDAR_AUTH_TOKEN = '1';
const EARNING_CALENDAR_PRIVATE_TOKEN = 'eyJ0eXAiOiJKV';
const EARNING_CALENDAR_UPDATE_EXCLUDE = [
    'assets',
    'client',
    'updater',
    '.git',
    '.gitignore',
    'tables',
    'configs',
];