<?php

namespace Earning_Calendar\App\Controllers;

use Earning_Calendar\App\JWT\Earning_Calendar_JWT;

class Earning_Calendar_File_Controller
{

    protected $config;
    protected $data;

    protected function __construct($config)
    {

        $this->config = $config;
        $this->data = $this->config->get_config();
        if (empty($this->data)) {
            Earning_Calendar_Error_Controller::getInstance()->add_error(
                'Config is empty',
                __FILE__ . ': ' . __LINE__,
                'config',
                'fatal');
        }
    }

    public static function download_file($file)
    {
        $opts = [
            'http' => [
                'method' => "GET",
                'header' => "token:".EARNING_CALENDAR_SERVER_AUTH_TOKEN."\r\n",
            ],
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
            ],
        ];
        $context = stream_context_create($opts);
        $response = get_headers($file, true, $context);

        if (!$response) {
            return Earning_Calendar_Error_Controller::getInstance()->add_error(
                'File -> "' . $file . '" <- not exist (server error) ',
                __FILE__ . ': ' . __LINE__,
                'download',
                'fatal'
            );
        }

        if (!strpos($response[0], '200')) {
            return Earning_Calendar_Error_Controller::getInstance()->add_error(
                'File -> "' . $file . '" <- not exist (' . $response[0] . ')',
                __FILE__ . ': ' . __LINE__,
                'download',
                'fatal'
            );
        }

        $response = file_get_contents($file, false, $context);

        if (!$response) {
            return Earning_Calendar_Error_Controller::getInstance()->add_error(
                'Can`t download "' . $file . '" file',
                __FILE__ . ': ' . __LINE__,
                'download',
                'fatal'
            );
        }
        return $response;
    }

    public static function save_file($file, $path_to_save, $is_file = false, $delete_old_file = false): bool
    {
        if ($is_file) {
            if (!file_exists($file)) {
                Earning_Calendar_Error_Controller::getInstance()->add_error(
                    'File "' . $file . '" isn`t exist',
                    __FILE__ . ': ' . __LINE__,
                    'files',
                    'fatal'
                );
                return false;
            } else {
                $file = file_get_contents($file);
                if ($delete_old_file && $file) {
                    unlink($file);
                }
            }
        }
        $array = explode('/', $path_to_save);
        $dir = str_replace(end($array),'', $path_to_save);
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
        $result = file_put_contents($path_to_save, $file, LOCK_EX);
        return (bool)$result;
    }

}