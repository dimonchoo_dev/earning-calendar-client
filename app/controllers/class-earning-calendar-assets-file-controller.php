<?php

namespace Earning_Calendar\App\Controllers;
use \ZipArchive;

class Earning_Calendar_Assets_File_Controller extends Earning_Calendar_File_Controller
{

    public function __construct($config)
    {
        parent::__construct($config);

    }

    public function download_files_from_config()
    {
        $response = parent::download_file($this->config->get_config());
        if (is_int($response)) {
            Earning_Calendar_Error_Controller::getInstance()->get_error('download', 'fatal', $response); // error
        } else {
            $clientDir = EARNING_CALENDAR_PLUGIN_PATH . 'client/';
            if (file_exists($clientDir . 'assets.zip')) {
                unlink($clientDir . 'assets.zip');
            }
            if (!file_exists($clientDir)) {
                mkdir($clientDir);
            }
            $path_to_file = $clientDir . 'assets.zip';
            Earning_Calendar_File_Controller:self::save_file($response, $path_to_file);
            if (file_exists($clientDir . 'assets.zip')) {
                $zip = new ZipArchive;
                if ($zip->open($clientDir . 'assets.zip') === TRUE) {
                    $zip->extractTo($clientDir);
                    $zip->close();
                }
            } else {
                Earning_Calendar_Error_Controller::getInstance()->add_error(
                    'Cannot find assets.zip',
                    __FILE__ . ': ' . __LINE__,
                    'assets',
                    'fatal'
                );
            }
        }

    }

}