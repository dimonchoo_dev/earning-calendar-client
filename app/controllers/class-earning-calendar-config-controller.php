<?php

namespace Earning_Calendar\App\Controllers;

use Earning_Calendar\App\Controllers\Earning_Calendar_Table_File_Controller,
    Earning_Calendar\App\Controllers\Earning_Calendar_Assets_File_Controller;

class Earning_calendar_Config_Controller
{

    private $configs = [
        'config' => Earning_Calendar_Table_File_Controller::class,
        'assets' => Earning_Calendar_Assets_File_Controller::class
    ];
    private $is_new_data = false;
    private $path_to_config;
    private $url_to_config;
    private $config_name;
    private $new_config;
    private $config;


    public function __construct($config_name, $download = true)
    {
        if (!array_key_exists($config_name, $this->configs)) {
            Earning_Calendar_Error_Controller::getInstance()->add_error(
                'Incorrect config name',
                __FILE__ . ': ' . __LINE__,
                'config',
                'fatal'
            );
            return;
        }
        $this->config_name = $config_name;
        $this->path_to_config = EARNING_CALENDAR_CONFIGS_PATH . $config_name . '.json';
        $this->url_to_config = $config_name === 'config' ? EARNING_CALENDAR_CONFIG_URL : EARNING_CALENDAR_ASSETS_URL;
        $this->init($download);
    }

    private function init($download)
    {
        if (file_exists($this->path_to_config)) {
            $config = file_get_contents($this->path_to_config);
            $this->config = json_decode($config);
        } else {
            $this->config = [];
            if ($download) {
                $result = $this->download_new_config();
                if ($result) {
                    $this->config = $this->new_config;
                } else {
                    Earning_Calendar_Error_Controller::getInstance()->add_error(
                        'Config is not found and cannot be loaded',
                        __FILE__ . ': ' . __LINE__,
                        'config',
                        'fatal'
                    );
                }
            }
        }
    }

    public function download_new_config(): bool
    {
        $new_config = Earning_Calendar_File_Controller::download_file($this->url_to_config);
        if (!is_int($new_config)) {
            $this->new_config = json_decode($new_config);
            if (property_exists($this->new_config, 'dateUpdate')) {
                return $this->update_config();
            } else {
                Earning_Calendar_Error_Controller::getInstance()->add_error(
                    'Incorrect data',
                    __FILE__ . ': ' . __LINE__,
                    'config',
                    'fatal'
                );
                return false;
            }
        } else {
            Earning_Calendar_Error_Controller::getInstance()->add_error(
                'Can`t download config',
                __FILE__ . ': ' . __LINE__,
                'config',
                'fatal'
            );
            return false;
        }
    }

    public function get_config($with_time = false)
    {
        if (!empty($this->config)) {
            if ($with_time) {
                return $this->config;
            } else {
                return $this->config->data;
            }
        } else {
            Earning_Calendar_Error_Controller::getInstance()->add_error(
                'Config is empty',
                __FILE__ . ': ' . __LINE__,
                'config',
                'warning'
            );
            return false;
        }
    }

    public function update_files()
    {
        if ($this->is_new_data && !Earning_Calendar_Error_Controller::getInstance()->is_fatal('config')) {
            $data = new $this->configs[$this->config_name]($this);
            $data->download_files_from_config();
            if ( function_exists('clean_redis') ) {
                clean_redis();
            }
        } else {
            Earning_Calendar_Error_Controller::getInstance()->add_error(
                'Config is up to date or trouble with config',
                __FILE__ . ': ' . __LINE__,
                'config',
                'message'
            );
        }
    }

    public function is_new_config(): bool
    {
        if ($this->new_config->dateUpdate !== $this->config->dateUpdate) {
            return true;
        } else {
            return false;
        }
    }

    public function is_new_data(): bool
    {
        return $this->is_new_data;
    }

    private function update_config(): bool
    {
        if (empty($this->config) || $this->is_new_config()) {
            $this->is_new_data = true;
            $this->config = $this->new_config;
            $json = json_encode($this->config);
            return Earning_Calendar_File_Controller::save_file($json, $this->path_to_config);
        } else {
            Earning_Calendar_Error_Controller::getInstance()->add_error(
                'Config is up to date',
                __FILE__ . ': ' . __LINE__,
                'config',
                'message'
            );
            return false;
        }
    }

}