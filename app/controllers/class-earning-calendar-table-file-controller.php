<?php

namespace Earning_Calendar\App\Controllers;

use \ZipArchive;

class Earning_Calendar_Table_File_Controller extends Earning_Calendar_File_Controller
{

    public function __construct($config)
    {
        parent::__construct($config);

    }

    public function download_files_from_config() {
        $dir = EARNING_CALENDAR_TEMPLATES_PATH;
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
        $config = $this->config->get_config(true);
        $response = Earning_Calendar_File_Controller::download_file($config->templatesZipArchive);
        if (is_int($response)) {
            Earning_Calendar_Error_Controller::getInstance()->get_error('download', 'fatal', $response); // error
        } else {
            $path_to_file = $dir . DIRECTORY_SEPARATOR . 'tables.zip';
            Earning_Calendar_File_Controller:
            self::save_file($response, $path_to_file);
            $zip = new ZipArchive;
            if ($zip->open($path_to_file) === TRUE) {
                $zip->extractTo($dir);
                $zip->close();
            }
        }

    }
    public function download_files_from_config_old()
    {
        foreach ($this->data as $name => $data) {
            foreach ($data as $table_name => $html_url) {
                $dir = EARNING_CALENDAR_TEMPLATES_PATH . $name;
                if (!file_exists($dir)) {
                    mkdir($dir, 0777, true);
                }

                if ($table_name !== 'options') {
                    $response = Earning_Calendar_File_Controller::download_file($html_url);
                    if (is_int($response)) {
                        Earning_Calendar_Error_Controller::getInstance()->get_error('download', 'fatal', $response); // error
                    } else {
                        $path_to_file = $dir . DIRECTORY_SEPARATOR . $table_name . '.html';
                        Earning_Calendar_File_Controller:
                        self::save_file($response, $path_to_file);
                    }
                }
            }
        }
    }
}