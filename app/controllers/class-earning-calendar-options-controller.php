<?php

namespace Earning_Calendar\App\Controllers;

use Earning_Calendar\App\Pattern\Singleton;

class Earning_Calendar_Options_Controller extends Singleton
{
    private $sqlite;
    private $db_name = EARNING_CALENDAR_PLUGIN_PATH . 'app/configs/options.db';

    public function __construct()
    {
        $this->sqlite = new \SQLite3($this->db_name);
        if (empty($this->select('earning_options', ['option_name' => 'replace']))) {
            $this->insert('earning_options', ['option_name' => 'replace', 'option_value' => serialize([])]);
        }
    }

    public function select(string $table_name, array $where_params = []): array
    {
        $sql = "SELECT * FROM {$table_name} ";
        $new_where_params = [];
        foreach ($where_params as $param_key => $param_value) {
            $new_where_params[] = $param_key . ' LIKE \'' . $param_value . "'";
        }
        if (!empty($new_where_params)) {
            $sql .= ' WHERE ' . implode(' AND ', $new_where_params);
        }

        $rows = [];
        $result = $this->sqlite->query($sql);

        while ($row = $result->fetchArray()) {
            $rows[] = $row;
        }

        return is_bool($rows) ? [] : $rows;
    }

    public function insert(string $table_name, array $params = []): \SQLite3Result
    {
        $sql = "INSERT INTO {$table_name} (" . implode(',', array_keys($params)) . ") VALUES ";
        $new_params = [];
        foreach ($params as $param_value) {
            $new_params[] = "'" . $param_value . "'";
        }
        $sql .= '(' . implode(',', $new_params) . ')';

        return $this->sqlite->prepare($sql)->execute();
    }

    public function update(string $table_name, array $params = [], array $where_params = []): \SQLite3Result
    {
        $sql = "UPDATE {$table_name} SET ";
        $new_params = [];
        $new_where_params = [];
        foreach ($params as $param_key => $param_value) {
            $new_params[] = $param_key . '=\'' . $param_value . '\'';
        }
        foreach ($where_params as $param_key => $param_value) {
            $new_where_params[] = $param_key . '="' . $param_value . '"';
        }
        $sql .= implode(',', $new_params);

        if (!empty($new_where_params)) {
            $sql .= ' WHERE ' . implode(' AND ', $new_where_params);
        }
        return $this->sqlite->prepare($sql)->execute();
    }

    public function delete(string $table_name, array $where_params = []): \SQLite3Result
    {
        $sql = "DELETE FROM {$table_name} WHERE ";
        $new_where_params = [];
        foreach ($where_params as $param_key => $param_value) {
            $new_where_params[] = $param_key . '="' . $param_value . '"';
        }
        $sql .= implode(' AND ', $new_where_params);

        return $this->sqlite->prepare($sql)->execute();
    }

    public function get_options($name = 'all'): array
    {
        if ($name === 'all') {
            $options = $this->select('earning_options');
        } else {
            $options = $this->select('earning_options', ['option_name'=> $name]);
        }

        $all_options = [];
        foreach ($options as $option) {
            $data = is_serialized($option['option_value']) ? unserialize($option['option_value']) :$option['option_value'];
            $all_options[$option['option_name']] = $data;
        }
        return $all_options;
    }

}