<?php

namespace Earning_Calendar\App\Controllers;

class Earning_Calendar_Shortcode_Controller
{

    private $config_controller;
    public $config;
    public $language;

    public function __construct()
    {

        $this->config_controller = new Earning_calendar_Config_Controller('config', false);
        $this->config = $this->config_controller->get_config();

    }

    public function register_shortcodes(): void
    {
        if (defined('POLYLANG_VERSION')) {
            $this->language = pll_current_language();
        } else {
            $lang = explode('_', get_locale());
            $this->language = $lang[0];
            unset($lang);
        }
        if (!$this->config) return;
        foreach ($this->config as $name => $table) {
            add_shortcode($name, function ($atts) use ($name, $table) {
                $options = Earning_Calendar_Options_Controller::getInstance()->get_options('replace');
                if ($atts) {
                    if (array_key_exists('data', $atts)) {
                        $file_name = $atts['data'];
                        $data_name = $atts['data'];
                        if (array_key_exists('with_lang', $atts) && $atts['with_lang'] === 'true') {
                            $lang = $this->language;
                        } elseif (array_key_exists('with_lang', $atts) && $atts['with_lang'] !== 'true') {
                            $lang = $atts['with_lang'];
                        }
                    } else {
                        if (array_key_exists('with_lang', $atts) && $atts['with_lang'] !== 'true') {
                            $file_name = $atts['with_lang'];
                        } else {
                            $file_name = $this->language;

                        }
                    }
                } else {
                    $file_name = '';
                }

                if (isset($lang)) {
                    $file_name = $lang . '_' . $file_name;
                }

                $file_path = EARNING_CALENDAR_TEMPLATES_PATH . $name . DIRECTORY_SEPARATOR . $file_name . '.html';

                if (!file_exists($file_path)) {
                    if (defined('POLYLANG_VERSION')) {
                        $lang = pll_default_language();
                        if (isset($data_name)) {
                            $file_path = EARNING_CALENDAR_TEMPLATES_PATH . $name . DIRECTORY_SEPARATOR . $lang . '_' . $data_name . '.html';
                        } else {
                            $file_path = EARNING_CALENDAR_TEMPLATES_PATH . $name . DIRECTORY_SEPARATOR . $lang . '.html';

                        }
                        if (!file_exists($file_path)) {
                            if (isset($data_name)) {
                                $file_path = EARNING_CALENDAR_TEMPLATES_PATH . $name . DIRECTORY_SEPARATOR . 'en_' . $data_name . '.html';
                            } else {
                                $file_path = EARNING_CALENDAR_TEMPLATES_PATH . $name . DIRECTORY_SEPARATOR . 'en' . '.html';

                            }
                            if (!file_exists($file_path)) {
                                return '';
                            }

                        }
                    } else {
                        $file_path = EARNING_CALENDAR_TEMPLATES_PATH . $name . DIRECTORY_SEPARATOR . 'en' . '.html';
                        if (!file_exists($file_path)) {
                            return '';
                        }
                    }
                }
                $response = file_get_contents($file_path);
                if (array_key_exists('replace', $options)) {
                    foreach ($options['replace'] as $search => $replace) {
                        $response = str_replace($search, $replace, $response);
                    }
                }

                if ($name === 'earningSeason') {
                    $replace = [];
                    preg_match_all('/src="(.*?)"/', $response, $matches);
                    foreach ($matches[1] as $match) {
                        $img = EARNING_CALENDAR_PLUGIN_URL . 'client/public/assets/image/brand-icons' . $match;
                        if (!in_array($match, $replace)) {
                            $response = str_replace($match, $img, $response);
                            $replace[] = $match;
                        }
                    }
                }
                return $response ?: '';
            });
        }

    }

    public function register_spread_table(): void
    {
        add_shortcode('spreadTableAll', function ($atts) {
            $atts = shortcode_atts([
                'data' => 'all',
                'url' => '',
                'currencies' => 'currencies'
            ], $atts);

            ?>
            <?php if ($atts['data'] === 'all' || $atts['data'] === 'web'): ?>
                <div class="webplatform <?= $atts['currencies'] ?>">
                    <div class="best-deals-table">
                        <?= do_shortcode("[spreadTable data=${atts['url']}currencies_web]") ?>
                    </div>
                </div>
            <?php
            endif;
            if ($atts['data'] === 'all' || $atts['data'] === 'mt4'):
                ?>
                <div class="mt4 <?= $atts['currencies'] ?>">
                    <div class="best-deals-table">
                        <?= do_shortcode("[spreadTable data=${atts['url']}currencies_mt4]") ?>
                    </div>
                </div>
            <?php
            endif;
            if ($atts['data'] === 'all' || $atts['data'] === 'web'):
                ?>
                <div class="webplatform indices">
                    <div class="best-deals-table">
                        <?= do_shortcode("[spreadTable data=${atts['url']}cfd_web]") ?>
                    </div>
                </div>
            <?php
            endif;
            if ($atts['data'] === 'all' || $atts['data'] === 'mt4'):
                ?>
                <div class="mt4 indices">
                    <div class="best-deals-table">
                        <?= do_shortcode("[spreadTable data=${atts['url']}cfd_mt4]") ?>
                    </div>
                </div>
            <?php
            endif;
            if ($atts['data'] === 'all' || $atts['data'] === 'web'):
                ?>
                <div class="webplatform americanstocks">
                    <div class="best-deals-table">
                        <?= do_shortcode("[spreadTable data=${atts['url']}american_web]") ?>
                    </div>
                </div>
            <?php
            endif;
            if ($atts['data'] === 'all' || $atts['data'] === 'mt4'):
                ?>
                <div class="mt4 americanstocks">
                    <div class="best-deals-table">
                        <?= do_shortcode("[spreadTable data=${atts['url']}american_mt4]") ?>
                    </div>
                </div>
            <?php
            endif;
            if ($atts['data'] === 'all' || $atts['data'] === 'web'):
                ?>
                <div class="webplatform  etffunds">
                    <div class="best-deals-table">
                        <?= do_shortcode("[spreadTable data=${atts['url']}etf_web]") ?>
                    </div>
                </div>
            <?php
            endif;
            if ($atts['data'] === 'all' || $atts['data'] === 'mt4'):
                ?>
                <div class="mt4 etffunds">
                    <div class="best-deals-table">
                        <?= do_shortcode("[spreadTable data=${atts['url']}etf_mt4]") ?>
                    </div>
                </div>
            <?php
            endif;
            if ($atts['data'] === 'all' || $atts['data'] === 'web'):
                ?>
                <div class="webplatform commodities">
                    <div class="best-deals-table">
                        <?= do_shortcode("[spreadTable data=${atts['url']}commodities_web]") ?>
                    </div>
                </div>
            <?php
            endif;
            if ($atts['data'] === 'all' || $atts['data'] === 'mt4'):
                ?>
                <div class="mt4 commodities">
                    <div class="best-deals-table">
                        <?= do_shortcode("[spreadTable data=${atts['url']}commodities_mt4]") ?>
                    </div>
                </div>
            <?php
            endif;
            if ($atts['data'] === 'all' || $atts['data'] === 'web'):
                ?>
                <div class="webplatform europeanstocks">
                    <div class="best-deals-table">
                        <?= do_shortcode("[spreadTable data=${atts['url']}european_web]") ?>
                    </div>
                </div>
            <?php
            endif;
            if ($atts['data'] === 'all' || $atts['data'] === 'mt4'):
                ?>
                <div class="mt4 europeanstocks">
                    <div class="best-deals-table">
                        <?= do_shortcode("[spreadTable data=${atts['url']}european_mt4]") ?>
                    </div>
                </div>
            <?php
            endif;
            if ($atts['data'] === 'all' || $atts['data'] === 'web'):
                ?>
                <div class="webplatform arabianstocks">
                    <div class="best-deals-table">
                        <?= do_shortcode("[spreadTable data=${atts['url']}arabian]") ?>
                    </div>
                </div>
            <?php
            endif;
            if ($atts['data'] === 'all' || $atts['data'] === 'mt4'):
                ?>
                <div class="mt4 arabianstocks">
                    <div class="best-deals-table">
                        <?= do_shortcode("[spreadTable data=${atts['url']}arabian]") ?>
                    </div>
                </div>
            <?php
            endif;
            if ($atts['data'] === 'all' || $atts['data'] === 'web'):
                ?>
                <div class="webplatform chinesestocks">
                    <div class="best-deals-table">
                        <?= do_shortcode("[spreadTable data=${atts['url']}chinese]") ?>
                    </div>
                </div>
            <?php
            endif;
            if ($atts['data'] === 'all' || $atts['data'] === 'mt4'):
                ?>
                <div class="mt4 chinesestocks">
                    <div class="best-deals-table">
                        <?= do_shortcode("[spreadTable data=${atts['url']}chinese]") ?>
                    </div>
                </div>
            <?php
            endif;
            if ($atts['data'] === 'all' || $atts['data'] === 'web'):
                ?>
                <div class="webplatform russianstocks">
                    <div class="best-deals-table">
                        <?= do_shortcode("[spreadTable data=${atts['url']}russian_web]") ?>
                    </div>
                </div>
            <?php
            endif;
            if ($atts['data'] === 'all' || $atts['data'] === 'mt4'):
                ?>
                <div class="mt4 russianstocks">
                    <div class="best-deals-table">
                        <?= do_shortcode("[spreadTable data=${atts['url']}russian_mt4]") ?>
                    </div>
                </div>
            <?php
            endif;
            if ($atts['data'] === 'all' || $atts['data'] === 'web'):
                ?>
                <div class="webplatform cryptocurrencies">
                    <div class="best-deals-table">
                        <?= do_shortcode("[spreadTable data=${atts['url']}cryptocurrency_web]") ?>
                    </div>
                </div>
            <?php
            endif;
            if ($atts['data'] === 'all' || $atts['data'] === 'mt4'):
                ?>
                <div class="mt4 cryptocurrencies">
                    <div class="best-deals-table">
                        <?= do_shortcode("[spreadTable data=${atts['url']}cryptocurrency_mt4]") ?>
                    </div>
                </div>
            <?php
            endif;
        });
    }

    public function register_selector_for_spreads(): void
    {
        add_shortcode('registerSelectorForSpreads', function ($atts) {
            $atts = shortcode_atts([
                'data' => 'all',
                'currencies' => 'currencies'
            ], $atts);
            ?>
            <div class="table-select__wrapper">
                <div class="select-wr">
                    <p class="select__description">Platform:</p>

                    <select class="select__body" id="platforms">
                        <?php if ($atts['data'] === 'web' || $atts['data'] === 'all'): ?>
                            <option value="webplatform">Web platform</option>
                        <?php
                        endif;
                        if ($atts['data'] === 'mt4' || $atts['data'] === 'all'):
                            ?>
                            <option value="mt4">МТ4</option>
                        <?php endif; ?>
                    </select>

                </div>
                <div class="select-wr">
                    <p class="select__description">Tool group:</p>

                    <select class="select__body" id="instruments">
                        <option value="<?= $atts['currencies'] ?>">Currencies</option>
                        <option value="indices">Indexes</option>
                        <option value="americanstocks">American stocks</option>
                        <option value="etffunds">ETF funds</option>
                        <option value="commodities">Commodities</option>
                        <option value="europeanstocks">European stocks</option>
                        <option value="arabianstocks">Arabian stocks</option>
                        <option value="chinesestocks">Chinese stocks</option>
                        <option value="russianstocks">Russian stocks</option>
                        <option value="cryptocurrencies">Cryptocurrencies</option>
                    </select>
                </div>
            </div>
            <?php
        });
    }

    public function register_hard_percent(): void
    {
        add_shortcode('hardPercentCalculator', function () {
            ?>
            <style>
                .hard-percent {
                    display: flex;
                    flex-direction: row;
                    gap: 20px;
                    max-width: 1200px !important;
                }

                @media (max-width: 900px) {
                    .hard-percent {
                        flex-direction: column;
                        align-items: center;
                    }
                }

                .hard-percent_form {
                    display: flex;
                    flex-direction: column;
                    align-self: baseline;
                    gap: 20px;
                    max-width: 440px;
                    padding: 20px;
                    background: #f0f0f2;
                    flex-shrink: 0;
                }

                .hard-percent label {
                    margin-bottom: 7px;
                    display: block;
                }

                .hard-percent input {
                    width: 100%;
                }

                .hard-percent input {
                    height: 46px;
                    box-sizing: border-box;
                    padding: 10px 5px 10px 10px;
                    border: 1px solid #a7a8af;
                }

                .hard-percent input.hard-selector_value {
                    background: #fafbfc;
                    outline: none;
                    cursor: pointer;
                }

                .hard-percent input.no-border {
                    border-right: none;
                }

                .hard-percent_start {
                    display: flex;
                    flex-direction: column;
                }

                .hard-percent_sum,
                .hard-percent_percent {
                    border-radius: 4px;
                    -webkit-box-shadow: inset 1px 1px 2px rgba(0, 0, 0, .19);
                    box-shadow: inset 1px 1px 2px rgba(0, 0, 0, .19);
                    position: relative;
                    background: #fafafa;
                    padding: 30px 20px 20px;
                }

                .hard-selector {
                    position: relative;
                }

                .hard-selector_list {
                    display: none;
                }

                .hard-selector.open .hard-selector_list {
                    display: block;
                    position: absolute;
                    top: 46px;
                    z-index: 10;
                    width: 100%;
                    background: white;
                    border: 1px solid #a7a8af;
                }

                .hard-selector.open .hard-selector_list__item {
                    padding: 5px 18px;
                    cursor: pointer;
                }

                .hard-selector.open .hard-selector_list__item:hover {
                    background: #fafbfc;
                }

                .hard-percent_sum,
                .hard-percent_percent {
                    display: flex;
                    gap: 30px;
                }

                .hard-percent_sum__title,
                .hard-percent_percent__title {
                    position: absolute;
                    top: -13px;
                    background: #fafafa;
                    padding: 5px 7px;
                    font-size: 14px;
                    box-shadow: inset 0 1px 1px rgba(0, 0, 0, .15);
                    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .15);
                }

                .hard-percent_sum__wrapper,
                .hard-percent_percent__wrapper {
                    flex: 1 1 100%;
                    display: flex;
                    flex-direction: column;
                }

                .hard-percent_input__wrapper,
                .hard-percent_input__wrapper {
                    width: 100%;
                    display: flex;
                    flex-direction: row;
                }

                .hard-percent_addon {
                    flex: 1 0 46px;
                    height: 46px;
                    border: 1px solid #a7a8af;
                    border-left: none;
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    font-size: 18px;
                    background: #fafbfc;
                }

                .important-text {
                    color: #c7254e;
                }

                .hard-percent_btn {
                    display: flex;
                    justify-content: space-between;
                }

                .hard-percent_btn button {
                    border-radius: 2px;
                    -webkit-box-shadow: 0 1px 1px rgba(90, 90, 90, .10);
                    box-shadow: 0 1px 1px rgba(90, 90, 90, .10);
                    font-weight: 500;
                    padding: 10px 16px;
                    font-size: 18px;
                    outline: 0 !important;
                    box-sizing: border-box;
                    width: 120px;
                    border: 1px solid #e0e0e0;
                    cursor: pointer;
                }

                .hard-percent_btn__reset {
                    color: #000 !important;
                    background-color: #fff;
                    border-color: #e0e0e0;
                }

                .hard-percent_btn__submit {
                    background-color: #065af7;
                    border-color: #0555eb;
                    color: #fff;
                }

                .caret {
                    display: inline-block;
                    width: 0;
                    height: 0;
                    margin-left: 2px;
                    vertical-align: middle;
                    border-top: 4px dashed;
                    border-right: 4px solid transparent;
                    border-left: 4px solid transparent;
                    background: #fafbfc;
                    position: absolute;
                    right: 5px;
                    top: 50%;
                    transform: translateY(-50%);
                    pointer-events: none;
                }

                .hard-percent_result {
                    flex: 1 1 100%;
                    display: flex;
                    flex-direction: column;
                    align-self: normal;
                }

                .hard-percent_info {
                    display: flex;
                    flex-wrap: wrap;
                    gap: 30px;
                }

                .hard-percent_info__replenishment,
                .hard-percent_info__percent,
                .hard-percent_info__total {
                    display: flex;
                    flex-direction: column;
                    justify-content: center;
                    align-items: center;
                    gap: 15px;
                    padding: 20px;
                    box-sizing: border-box;
                }

                .hard-percent_info__total {
                    width: 100%;
                }

                .hard-percent_info__replenishment,
                .hard-percent_info__percent {
                    flex: 1 1 calc(50% - 15px);
                    background: #f2f2f2;
                }

                .hard-percent_info__total___text {
                    font-size: 18px;
                }

                .hard-percent_info__replenishment___text,
                .hard-percent_info__percent___text {
                    font-size: 14px;
                }

                .hard-percent_info__total___sum {
                    font-size: 50px;
                    font-weight: 600;
                }

                .hard-percent_info__percent___sum,
                .hard-percent_info__replenishment___sum {
                    font-size: 32px;
                    font-weight: 600;
                }
            </style>
            <div class="hard-percent">
                <form class="hard-percent_form" novalidate>
                    <div class="hard-percent_start">
                        <label for="hardStartSum"><span class="important-text">Шаг 1</span> Первоначальная сумма</label>
                        <div class="hard-percent_input__wrapper">
                            <input value="0" id="hardStartSum" class="no-border hard-percent_start_input" type="number">
                            <div class="hard-percent_addon hard-selector currencies">
                                <input value="₽" type="text" readonly class="hard-selector_value no-border">
                                <span class="caret"></span>
                                <div class="hard-selector_list">
                                    <div class="hard-selector_list__item">$</div>
                                    <div class="hard-selector_list__item">€</div>
                                    <div class="hard-selector_list__item">£</div>
                                    <div class="hard-selector_list__item">₩</div>
                                    <div class="hard-selector_list__item">¥</div>
                                    <div class="hard-selector_list__item">Ұ</div>
                                    <div class="hard-selector_list__item">元</div>
                                    <div class="hard-selector_list__item">د.إ</div>
                                    <div class="hard-selector_list__item">₹</div>
                                    <div class="hard-selector_list__item">R$</div>
                                    <div class="hard-selector_list__item">฿</div>
                                    <div class="hard-selector_list__item">₽</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="hard-percent_sum">
                        <div class="hard-percent_sum__title">
                            <span class="important-text">Шаг 2</span><span> Пополнения</span>
                        </div>
                        <div class="hard-percent_sum__wrapper">
                            <label for="hardSum">Сумма</label>
                            <div class="hard-percent_input__wrapper">
                                <input value="0" class="no-border hard-percent_sum__input" name="percent" id="hardSum"
                                       type="number">
                                <span class="hard-percent_addon currencies_change">₽</span>
                            </div>
                        </div>
                        <div class="hard-percent_sum__wrapper">
                            <label for="hardSumPeriod">Периодичность</label>
                            <div class="hard-percent_sum__selector hard-selector">
                                <input name="sumPeriod" data-value="12" value="каждый год" id="hardSumPeriod" readonly
                                       class="hard-selector_value">
                                <span class="caret"></span>
                                <div class="hard-selector_list">
                                    <div class="hard-selector_list__item" data-value="12">каждый год</div>
                                    <div class="hard-selector_list__item" data-value="4">каждое полугодие</div>
                                    <div class="hard-selector_list__item" data-value="2">каждый квартал</div>
                                    <div class="hard-selector_list__item" data-value="1">каждый месяц</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="hard-percent_percent">
                        <div class="hard-percent_percent__title">
                            <span class="important-text">Шаг 3</span><span> Начисления процентов</span>
                        </div>
                        <div class="hard-percent_percent__wrapper">
                            <label for="hardPercent">Процент</label>
                            <div class="hard-percent_input__wrapper">
                                <input value="0" class="no-border hard-percent_percent__input" name="percent"
                                       id="hardPercent" type="number">
                                <span class="hard-percent_addon">%</span>
                            </div>
                        </div>
                        <div class="hard-percent_percent__wrapper">
                            <label for="hardPercentPeriod">Периодичность</label>
                            <div class="hard-percent_percent__selector hard-selector">
                                <input name="hardPercentPeriod" data-value="12" value="каждый год"
                                       id="hardPercentPeriod" readonly class="hard-selector_value">
                                <span class="caret"></span>
                                <div class="hard-selector_list">
                                    <div class="hard-selector_list__item" data-value="12">каждый год</div>
                                    <div class="hard-selector_list__item" data-value="42">каждое полугодие</div>
                                    <div class="hard-selector_list__item" data-value="2">каждый квартал</div>
                                    <div class="hard-selector_list__item" data-value="1">каждый месяц</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="hard-percent_year">
                        <label for="hardYear"><span class="important-text">Шаг 4</span> Количество лет</label>
                        <div class="hard-percent_input__wrapper">
                            <input class="hard-percent_year_input" min="1" id="hardYear" type="number" value="1">
                        </div>
                    </div>
                    <div class="hard-percent_btn">
                        <button class="hard-percent_btn__reset" type="button">Сбросить</button>
                        <button class="hard-percent_btn__submit" type="button">Считать</button>
                    </div>
                </form>
                <div class="hard-percent_result">
                    <div class="hard-percent_info">
                        <div class="hard-percent_info__total">
                            <span class="hard-percent_info__total___text">Итоговый баланс</span>
                            <span class="hard-percent_info__total___sum">0 ₽</span>
                        </div>
                        <div class="hard-percent_info__replenishment">
                            <span class="hard-percent_info__replenishment___sum">0 ₽</span>
                            <span class="hard-percent_info__replenishment___text">Всего пополнений</span>
                        </div>
                        <div class="hard-percent_info__percent">
                            <span class="hard-percent_info__percent___sum">0 ₽</span>
                            <span class="hard-percent_info__percent___text">Всего процентов</span>
                        </div>
                    </div>
                    <canvas id="myChart" width="400" height="400"></canvas>
                </div>
            </div>
            <script>
                const hardCalc = document.querySelector('.hard-percent_form');
                const ctx = document.getElementById('myChart').getContext('2d');
                let myChart;
                if (hardCalc) {
                    const selectors = hardCalc.querySelectorAll('.hard-selector');
                    const submitBtn = hardCalc.querySelector('.hard-percent_btn__submit');
                    const resetBtn = hardCalc.querySelector('.hard-percent_btn__reset');
                    const inputs = hardCalc.querySelectorAll('.hard-percent_year_input, .hard-percent_start_input, .hard-percent_sum__input, .hard-percent_percent__input');
                    window.addEventListener('mousemove', () => {
                        const src = 'https://cdn.jsdelivr.net/npm/chart.js@3.6.0/dist/chart.min.js';
                        const script = document.createElement('script');
                        script.src = src;
                        document.head.appendChild(script);
                        script.onload = () => {
                            const data = {
                                labels: [1, 2, 3, 4, 5],
                                datasets: [
                                    {
                                        label: 'Начальная сумма',
                                        data: [100, 100, 100, 100, 100],
                                        backgroundColor: 'rgb(6, 78, 164)',
                                    },
                                    {
                                        label: 'Пополнение',
                                        data: [100, 200, 300, 400, 500],
                                        backgroundColor: 'rgb(40, 139, 255)',
                                    },
                                    {
                                        label: 'Начисленные проценты',
                                        data: [20, 51, 92.2, 156.92, 232.612],
                                        backgroundColor: 'rgb(79, 205, 48)',
                                    },
                                ]
                            };
                            const config = {
                                type: 'bar',
                                data: data,
                                options: {
                                    plugins: {
                                        title: {
                                            display: false,
                                        },
                                    },
                                    responsive: true,
                                    scales: {
                                        x: {
                                            stacked: true,
                                        },
                                        y: {
                                            stacked: true
                                        }
                                    }
                                }
                            };
                            myChart = new Chart(ctx, config);
                        }
                    }, {once: true});

                    selectors.forEach(selector => {
                        const valueBlock = selector.querySelector('.hard-selector_value');
                        const listBlock = selector.querySelector('.hard-selector_list');

                        const itemBlocks = listBlock.querySelectorAll('.hard-selector_list__item');
                        valueBlock.addEventListener('click', () => {
                            selector.classList.toggle('open')
                        })
                        window.addEventListener('click', evt => {
                            if (evt.target.closest('.hard-selector') !== selector) {
                                selector.classList.remove('open')
                            }
                        })
                        itemBlocks.forEach(item => {
                            item.addEventListener('click', () => {
                                valueBlock.value = item.innerHTML
                                valueBlock.dataset.value = item.dataset.value
                                selector.classList.remove('open')
                                if (selector.classList.contains('currencies')) {
                                    document.querySelector('.hard-percent_addon.currencies_change').innerHTML = item.innerHTML;
                                }
                            })
                        })
                    })
                    inputs.forEach(input => {
                        input.addEventListener('blur', () => {
                            if (input.value < 0) {
                                input.value = 0;
                            }
                            if (input.value <= 0 && input.classList.contains('hard-percent_year_input')) {
                                input.value = 1;
                            }
                        })
                    })
                    resetBtn.addEventListener('click', ()=> {
                        inputs.forEach(input => {
                            if (input.id === 'hardYear') {
                                input.value = '1';
                            } else {
                                input.value = '0';
                            }
                        });
                    })

                    submitBtn.addEventListener('click', () => {
                        const hardCalc = document.querySelector('.hard-percent_form');
                        const startSum = parseInt(hardCalc.querySelector('input.hard-percent_start_input').value);
                        const repSum = parseInt(hardCalc.querySelector('input.hard-percent_sum__input').value);
                        const repSumPeriod = parseInt(hardCalc.querySelector('input#hardSumPeriod').dataset.value);
                        const repPercent = parseInt(hardCalc.querySelector('input.hard-percent_percent__input').value);
                        const repPercentPeriod = parseInt(hardCalc.querySelector('input#hardPercentPeriod').dataset.value);
                        const year = parseInt(hardCalc.querySelector('input.hard-percent_year_input').value);
                        let sum = startSum;
                        const period = year * 12;
                        let yearSum = []
                        let repArray = []
                        let perArray = []
                        let startArray = []
                        yearSum.push(sum);
                        let rep = 0;
                        let perForChart = 0;
                        for (let count = 1; count <= period; count++) {
                            if (count % repSumPeriod === 0 && repSum > 0) {
                                rep += repSum;
                                sum += repSum;
                            }
                            if (count % 12 === 0) {
                                perForChart = sum * (1 + repPercent / 100) - sum + perForChart;
                                perArray.push(perForChart);
                                repArray.push(rep)
                                yearSum.push(sum.toFixed(2));
                                startArray.push(startSum);
                            }
                            if (count % repPercentPeriod === 0 && repSum > 0) {
                                sum *= (1 + repPercent / 100);
                            }
                        }
                        const per = (sum - rep - startSum).toFixed(2);
                        const currencies = document.querySelector('.hard-percent_addon.currencies_change').innerHTML;
                        document.querySelector('.hard-percent_info__total___sum').innerHTML = sum.toFixed().toString() + ' ' + currencies;
                        document.querySelector('.hard-percent_info__replenishment___sum').innerHTML = rep.toString() + ' ' + currencies;
                        document.querySelector('.hard-percent_info__percent___sum').innerHTML = parseInt(per).toFixed() + ' ' + currencies;
                        renderChart(startArray, repArray, perArray);
                    });

                }

                function renderChart(data1, data2, data3) {
                    let labels = [];
                    for (let counter = 1; counter <= data1.length; counter++) {
                        labels.push(counter);
                    }

                    removeData();
                    addData(labels, data1, data2, data3);
                }

                function removeData() {
                    myChart.data.labels.pop();
                    myChart.data.datasets.forEach((dataset) => {
                        dataset.data = [];
                    });
                    myChart.update();
                }

                function addData(label, data1, data2, data3) {
                    console.log(data1);
                    console.log(data2);
                    console.log(data3);
                    console.log(myChart.data.datasets);
                    myChart.data.labels = label;
                    myChart.data.datasets[0].data = data1;
                    myChart.data.datasets[1].data = data2;
                    myChart.data.datasets[2].data = data3;
                    myChart.update();
                }
            </script>
            <?php
        });
    }
}