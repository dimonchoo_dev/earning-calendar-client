<?php

namespace Earning_Calendar\App\Controllers;

use Earning_Calendar\App\Pattern\Singleton;

class Earning_Calendar_Error_Controller extends Singleton
{
    private $errors = [];

    public function add_error(string $message, string $file, string $category = 'default', string $type = 'notice'): int
    {
        if (!isset($this->errors[$category][$type])) {
            $this->errors[$category][$type] = [];
        }
        $error = [
            'message' => $message,
            'file' => $file,
        ];
        if ($type === 'fatal') {
            $curl = curl_init(EARNING_CALENDAR_ERROR_LOG_URL);
            curl_setopt($curl, CURLOPT_URL, EARNING_CALENDAR_ERROR_LOG_URL);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $headers = array(
                "Accept: application/json",
                "Content-Type: application/json",
                "token:".EARNING_CALENDAR_SERVER_AUTH_TOKEN,
            );
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($error));
            curl_close($curl);
        }

        return array_push($this->errors[$category][$type], $error);
    }

    public function get_errors(): array
    {
        if (empty($this->errors)) {
            return [];
        } else {
            return $this->errors;
        }
    }

    /**
     * @param string $category
     * @param string $type
     * @param int $number
     * @return array
     */

    public function get_error(string $category = 'default', string $type = 'notice', int $number = 1): array
    {
        if (!empty($this->errors)) {
            if (isset($this->errors[$category][$type][$number - 1])) {
                return $this->errors[$category][$type][$number - 1];
            }
        }
        return [];
    }

    /**
     * @param string $category
     * @return bool
     */

    public function is_fatal(string $category = 'default'): bool
    {
        if (array_key_exists($category, $this->errors)) {
            if (array_key_exists('fatal', $this->errors[$category])) {
                return true;
            }
        }
        return false;
    }

}