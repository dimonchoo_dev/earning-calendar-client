<?php

namespace Earning_Calendar;
use Exception;

spl_autoload_register([new Autoloader(), 'autoload']);

class Autoloader
{

    private $name_spaces = [
            'Earning_Calendar' => '',
            'Earning_Calendar\Includes' => 'includes',
            'Earning_Calendar\Admin' => 'admin',
            'Earning_Calendar\Client' => 'public',
            'Earning_Calendar\App' => 'app',
            'Earning_Calendar\App\JWT' => 'app'.DIRECTORY_SEPARATOR.'jwt',
            'Earning_Calendar\App\Controllers' => 'app'.DIRECTORY_SEPARATOR.'controllers',
            'Earning_Calendar\App\Pattern' => 'app'.DIRECTORY_SEPARATOR.'pattern',
            'Earning_Calendar\Updater' => 'updater',
        ];

    /**
     * @throws Exception
     */
    public function autoload($class): void
    {

        $class_infos = explode('\\', $class);
        $class_name = end($class_infos);
        $name_space = str_replace("\\{$class_name}", '', $class);
        if (isset($this->name_spaces[$name_space])) {
            $file_name = str_replace('_', '-', $class_name);
            $file_name = strtolower($file_name);
            $file_name = "class-{$file_name}.php";
            $file_path = EARNING_CALENDAR_PLUGIN_PATH . $this->name_spaces[$name_space] . DIRECTORY_SEPARATOR . $file_name;
            if (file_exists($file_path)) {
                require_once $file_path;
            }
        }
    }

}