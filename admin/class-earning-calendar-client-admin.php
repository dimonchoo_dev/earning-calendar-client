<?php

namespace Earning_Calendar\Admin;

/**
 * The admin-specific functionality of the plugin.
 *
 * @since      1.0.0
 *
 * @package    Earning_Calendar_Client
 * @subpackage Earning_Calendar_Client/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Earning_Calendar_Client
 * @subpackage Earning_Calendar_Client/admin
 * @author     erikza@wizardsdev.com
 */
class Earning_Calendar_Client_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $earning_calendar_client    The ID of this plugin.
	 */
	private $earning_calendar_client;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @param      string    $earning_calendar_client       The name of this plugin.
	 * @param string $version    The version of this plugin.
	 *@since    1.0.0
	 */
	public function __construct(string $earning_calendar_client, string $version ) {

		$this->earning_calendar_client = $earning_calendar_client;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets and JavaScipt for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles_and_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Earning_Calendar_Client_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Earning_Calendar_Client_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

        if (in_array(get_current_screen()->id, array('toplevel_page_earning_calendar',))) {
            wp_enqueue_style($this->earning_calendar_client, EARNING_CALENDAR_PLUGIN_URL . 'admin/css/earning-calendar-client-admin.css', [], $this->version, 'all');
            wp_enqueue_script($this->earning_calendar_client, EARNING_CALENDAR_PLUGIN_URL . 'admin/js/earning-calendar-client-admin.js', [], $this->version, true);
            wp_localize_script( $this->earning_calendar_client, 'REST_API_data', array(
                'root'  => esc_url_raw( rest_url() ),
                'nonce' => wp_create_nonce( 'wp_rest' )
            ) );
        }

	}

    /**
     * Register the admin menu for the admin area.
     *
     * @since    1.0.0
     */
    public function admin_menu()
    {

        add_menu_page(
            'earning_calendar',
            'Earning calendar Settings',
            'manage_options',
            'earning_calendar',
            [$this, 'earning_calendar_options_page'],
            'dashicons-media-spreadsheet',
            2
        );
    }

    /**
     * Render plugin options templates
     *
     * @since    1.0.0
     */

    public function earning_calendar_options_page()
    {
        /* check user's permissions */
        if (!current_user_can('manage_options')) {
            wp_die('Unauthorized user');
        }

        require_once('partials/earning-calendar-client-admin-display.php');
    }

}
