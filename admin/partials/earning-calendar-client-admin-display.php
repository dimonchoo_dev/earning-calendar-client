<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://wizardsdev.com/
 * @since      1.0.0
 *
 * @package    earning_calendar
 * @subpackage earning_calendar/admin/partials
 */

use Earning_Calendar\App\Controllers\Earning_Calendar_Options_Controller,
    Earning_Calendar\App\Controllers\Earning_calendar_Config_Controller,
    Earning_Calendar\App\JWT\Earning_Calendar_JWT;

?>

<?php
if (is_plugin_active('polylang/polylang.php')) {
    $translations = pll_languages_list();
}
$config = new Earning_calendar_Config_Controller('config', false);
$data = $config->get_config(true);
$options = Earning_Calendar_Options_Controller::getInstance()->get_options();
$jwt = new Earning_Calendar_JWT(EARNING_CALENDAR_PRIVATE_TOKEN, 'HS256', 3600, 0);
$token = ($jwt->encode(['key' => EARNING_CALENDAR_AUTH_TOKEN, 'scopes' => ['updater']]));
?>

<div class="wrap earning-calendar">

    <h1 style="font-weight: normal">Settings for wizards earning-calendar</h1>

    <div class="tabs">
        <div class="button tab-button" data-tab="info">Список таблиц и информация о них</div>
        <div class="button tab-button" data-tab="settings">Настройки</div>
        <div class="button tab-button" data-tab="manual_parse">Ручной запуск парсинга</div>
        <div class="button tab-button" data-tab="manual_update">Ручной запуск обновления плагина</div>
    </div>
    <div class="settings_tab">
        <div class="info">
            <table>
                <thead>
                <tr>
                    <td>Таблицы</td>
                    <td>Шорткод</td>
                    <td>Ссылка на google sheets</td>
                </tr>
                </thead>
                <tbody>
                <?php
                if ($data && property_exists($data, 'data')) {
                    foreach ($data->data as $name => $table): ?>

                        <tr>
                            <td><?= $name ?></td>
                            <td><?php
                                foreach ($table as $key => $value) {
                                    if ($key === 'options') continue;
                                    echo "[{$name} data={$key}]<br>";
                                }
                                ?>
                            </td>
                            <td>
                                <a href="<?= $table->options->googleSheetsUrl ?>"><?= $table->options->googleSheetsUrl ?></a>
                            </td>
                        </tr>
                    <?php endforeach;
                } else {
                    echo "<tr><td>Надо</td><td>спарсить</td><td>конфиг)</td></tr>";
                }
                ?>
                </tbody>
            </table>
        </div>
        <div class="settings" style="display: none">
            <h2>Замена слов</h2>
            <form class="replace">
                <div class="replace-wrapper">
                    <div class="replace-list">
                        <?php if ($options['replace']):
                            foreach ($options['replace'] as $search => $replace): ?>
                                <div class="replace-item <?= $search ?>definedFoaQcnjgr">
                                    <input name="replace[]" placeholder="Слово которое ищем" value="<?= htmlentities($search) ?>"
                                           type="text">
                                    <input name="replace_to[]" placeholder="Заменить на которое заменяем"
                                           value="<?= htmlentities($replace) ?>" type="text">
                                    <div class="replace-item-remove" data-item="<?= $search ?>definedFoaQcnjgr">
                                        <p>+</p>
                                    </div>
                                </div>
                            <?php endforeach;
                        endif;
                        ?>
                    </div>
                    <div type="button" class="replace-add">+</div>
                </div>
                <input type="submit" value="Созранить" class="button button-primary updateReplace">
            </form>
        </div>
        <div class="manual_parse" style="display: none">
            <button class="button button-primary updateData">Обновить данные</button>
            <button class="button button-primary updateAssets">Обновить Assets</button>
        </div>
        <div class="manual_update" style="display: none">
            <button class="button button-primary updatePlugin" data-token="<?= $token ?>">Обновить плагин</button>
            <button class="button button-primary rollbackPlugin" data-token="<?= $token ?>">Восстановить плагин</button>
        </div>
    </div>
</div>
