const mainWrapper = document.querySelector('.wrap.earning-calendar');
const tabButtons = document.querySelectorAll('.tab-button');
const settingsTab = document.querySelector('.settings_tab');
const updateData = document.querySelector('.updateData');
const updateAssets = document.querySelector('.updateAssets');
const replaceAdd = document.querySelector('.replace-add');
const replaceList = document.querySelector('.replace-list');
const replaceItemsRemove = document.querySelectorAll('.replace-item-remove');
const updateReplace = document.querySelector('.updateReplace');
const updatePlugin = document.querySelector('.updatePlugin');
const rollbackPlugin = document.querySelector('.rollbackPlugin');
let urlAlphabet = 'ModuleSymbhasOwnPrABCDEFGHNRVfgctiUvz_KqYTJkLxpZXIjQW';
let popupList = [];
let counter = 0;

tabButtons.forEach(button => {
    button.addEventListener('click', () => {
        for (let child of settingsTab.children) {
            if (child.classList.contains(button.dataset.tab)) {
                child.style.display = 'block';
            } else {
                child.style.display = 'none';
            }
        }
    })
})

let nanoid = (size = 21) => {
    let id = ''
    let i = size
    while (i--) {
        id += urlAlphabet[(Math.random() * 64) | 0]
    }
    return id
}

replaceAdd.addEventListener('click', addNewReplaceItem)

function addNewReplaceItem() {
    let hash = nanoid();
    let wrapper = document.createElement('div')
    let replace = document.createElement('input')
    let replaceTo = document.createElement('input')
    let remove = document.createElement('div')
    let removeIco = document.createElement('p')
    wrapper.classList.add('replace-item');
    wrapper.classList.add(hash);
    replace.name = 'replace[]';
    replace.placeholder = 'Слово которое ищем';
    replace.type = 'text';
    replaceTo.name = 'replace_to[]';
    replaceTo.placeholder = 'Заменить на которое заменяем';
    replaceTo.type = 'text';
    remove.classList.add('replace-item-remove');
    remove.dataset.item = hash;
    removeIco.innerText = '+';
    remove.appendChild(removeIco);
    wrapper.appendChild(replace);
    wrapper.appendChild(replaceTo);
    wrapper.appendChild(remove);
    remove.addEventListener('click', () => {
        removeReplaceItem(hash)
    });
    replaceList.appendChild(wrapper)
}

if (replaceItemsRemove) {
    replaceItemsRemove.forEach(element => {
        element.addEventListener('click', () => {
            removeReplaceItem(element.dataset.item);
        })

    })
}

function removeReplaceItem(hash) {
    replaceList.querySelector('.' + hash).remove();
}

const urls = {
    updateData: '/wp-json/earning_calendar/v1/update-data',
    saveReplace: '/wp-json/earning_calendar/v1/update-options',
    updatePlugin: '/wp-content/plugins/earning-calendar-client/updater/updater.php',
}

updateData.addEventListener('click', () => {
    let message = addMessage('Идет запрос на парсинг шаблонов');
    sendForm(JSON.stringify({action: 'config'}), urls.updateData, 'POST')
        .then(r => {
            console.log(r);
            message.classList.add('done')
            message.innerText = 'Done';
            setTimeout(()=> removeMessage(message), 1000);
        })
})

updatePlugin.addEventListener('click', () => {
    let message = addMessage('Идет обновление плагина');
    sendForm(JSON.stringify({action: 'update', token: updatePlugin.dataset.token}), urls.updatePlugin, 'POST')
        .then(r => {
            console.log(r);
            message.classList.add('done')
            message.innerText = 'Done';
            setTimeout(()=> removeMessage(message), 1000);
        })
})

rollbackPlugin.addEventListener('click', () => {
    let message = addMessage('Идет обновление плагина');
    sendForm(JSON.stringify({action: 'rollback', token: updatePlugin.dataset.token}), urls.updatePlugin, 'POST')
        .then(r => {
            console.log(r);
            message.classList.add('done')
            message.innerText = 'Done';
            setTimeout(()=> removeMessage(message), 1000);
        })
})

updateAssets.addEventListener('click', () => {
    let message = addMessage('Идет запрос на парсинг ассетсов');
    sendForm(JSON.stringify({action: 'assets'}), urls.updateData, 'POST')
        .then(r => {
            console.log(r);
            message.classList.add('done')
            message.innerText = 'Done';
            setTimeout(()=> removeMessage(message), 1000);
        })
})

updateReplace.addEventListener('click', e => {
    let message = addMessage('Идет запрос на парсинг ассетсов');
    e.preventDefault();
    let form = document.querySelector('.replace');
    let formData = new FormData(form);
    formData.append('action', 'replace');
    let data = formDataToJson(formData);

    sendForm(JSON.stringify(data), urls.saveReplace, 'POST')
        .then(r => {
            console.log(r);
            message.classList.add('done')
            message.innerText = 'Done';
            setTimeout(()=> removeMessage(message), 1000);
        })
})

function sendForm(data, url, method = 'POST') {
    const options = {
        method: method,
        body: data,
        headers: {
            'X-WP-Nonce': REST_API_data.nonce,
        },
    }
    return fetch(url, options)
}

function formDataToJson(formData) {
    let object = {};
    formData.forEach((value, key) => {
        if (key.indexOf('[]') !== -1) {
            key = key.replace(/\[]/gi, '');
            if (!object[key]) {
                object[key] = [];
            }
            object[key].push(value);
            return;
        }
        if (!Reflect.has(object, key)) {
            object[key] = value;
            return;
        }
        object[key].push(value);
    });
    return object;
}

const addMessage = (message, type = 'message')=> {
    const wrapper = document.createElement('div');
    wrapper.style.top = (80 * popupList.length + 20).toString() + 'px';
    wrapper.classList.add('message-popup');
    wrapper.id = 'number'+counter;
    counter++;
    wrapper.classList.add(type);
    wrapper.classList.add('hidden');
    wrapper.innerText = message;
    mainWrapper.appendChild(wrapper);
    wrapper.classList.remove('hidden');
    wrapper.classList.remove('view');
    popupList.push(wrapper);
    return wrapper;
}

const removeMessage = (block) => {
    block.classList.add('close');
    setTimeout(()=> {
        block.remove();
        popupList.find((element, index)=> {
            if (!element) return;
            if (element.id === block.id) {
                popupList.splice(index, 1);
            }
        })
    }, 1000)
}

