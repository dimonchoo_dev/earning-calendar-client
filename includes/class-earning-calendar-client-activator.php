<?php

namespace Earning_Calendar\Includes;

/**
 * Fired during plugin activation
 *
 * @since      1.0.0
 *
 * @package    Earning_Calendar_Client
 * @subpackage Earning_Calendar_Client/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Earning_Calendar_Client
 * @subpackage Earning_Calendar_Client/includes
 * @author     erikza@wizardsdev.com
 */
class Earning_Calendar_Client_Activator
{

    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     * @since    1.0.0
     */
    public static function activate()
    {
        if (!wp_next_scheduled('earning_calendar_cron_task')) {
            $time = 1631600000 + rand(64000 ,82000);
            wp_schedule_event($time, 'daily', 'earning_calendar_cron_task');
//            wp_schedule_event(1631664000, 'daily', 'earning_calendar_cron_task', ['assets']);
        }
    }

}
