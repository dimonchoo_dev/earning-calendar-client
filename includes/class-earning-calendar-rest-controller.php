<?php

namespace Earning_Calendar\Includes;

use Earning_Calendar\Includes\Earning_Calendar_Client,
    Earning_Calendar\App\Controllers\Earning_Calendar_Error_Controller,
    Earning_Calendar\App\Controllers\Earning_Calendar_Options_Controller,
    Earning_Calendar\App\JWT\Earning_Calendar_JWT;

class Earning_Calendar_Rest_Controller extends \WP_REST_Controller
{

    protected $namespace;
    private $client;
    public $jwt;

    function __construct($client)
    {
        $this->client = $client;
        $this->namespace = 'earning_calendar/v1';
        $this->jwt = new Earning_Calendar_JWT(EARNING_CALENDAR_PRIVATE_TOKEN, 'HS256', 3600, 0);
        define('EARNING_CALENDAR_SERVER_AUTH_TOKEN', ($this->jwt->encode(['key' => EARNING_CALENDAR_AUTH_TOKEN, 'scopes' => ['updater']])));
    }

    /**
     * Register all of the Rest request
     *
     * @since    1.0.0
     * @access   public
     */
    public function register_routes()
    {
        register_rest_route($this->namespace, "/update-data", [
            [
                'methods' => 'POST',
                'callback' => [$this, 'update_data'],
                'permission_callback' => [$this, 'earning_calendar_permission'],
            ]
        ]);
        register_rest_route($this->namespace, "/update-options", [
            [
                'methods' => 'POST',
                'callback' => [$this, 'update_options'],
                'permission_callback' => [$this, 'earning_calendar_permission'],
            ]
        ]);

    }

    function update_data($request)
    {
        $post = json_decode($request->get_body());
        if (gettype($post) === 'object') {
            if (property_exists($post, 'action')) {
                $response = $this->client->parse_data($post->action);
                if (!Earning_Calendar_Error_Controller::getInstance()->is_fatal()) {
                    return $response;
                }
            }
        }

        return new \WP_Error(400, 'Неправильный запрос');
    }

    function update_options($request)
    {
        $post = json_decode($request->get_body());
        if (gettype($post) === 'object') {
            if (property_exists($post, 'action')) {
                if ($post->action === 'replace') {
                    $options = [];
                    foreach ($post->replace as $key => $value) {
                        $options[$value] = $post->replace_to[$key];
                    }
                    $data = [
                        'option_name' => 'replace',
                        'option_value' => serialize($options)
                    ];
                    Earning_Calendar_Options_Controller::getInstance()->update('earning_options', $data);
                }
                return Earning_Calendar_Error_Controller::getInstance()->get_errors();
            }
        }

        return new \WP_Error(400, 'Неправильный запрос');
    }

    /**
     * The function that is responsible for checking the access level
     *
     * @since    1.0.0
     * @access   public
     */
    public function earning_calendar_permission(\WP_REST_Request $request)
    {
        if (!current_user_can('manage_options')) {
            $post = json_decode($request->get_body());

            if (property_exists($post, 'token') && $this->jwt->decode($post->token)['key'] != EARNING_CALENDAR_AUTH_TOKEN) {
                return new \WP_Error('rest_forbidden', esc_html__('You cannot view the post resource.'), ['status' => $this->error_status_code()]);
            }
        }

        return true;
    }

    public function error_status_code(): int
    {
        return is_user_logged_in() ? 403 : 401;
    }

}
