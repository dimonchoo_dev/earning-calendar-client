<?php

namespace Earning_Calendar\Includes;

/**
 * Fired during plugin deactivation
 *
 * @since      1.0.0
 *
 * @package    Earning_Calendar_Client
 * @subpackage Earning_Calendar_Client/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Earning_Calendar_Client
 * @subpackage Earning_Calendar_Client/includes
 * @author     erikza@wizardsdev.com
 */
class Earning_Calendar_Client_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
        wp_clear_scheduled_hook('earning_calendar_cron_task');
	}

}
