<?php
namespace Earning_Calendar\Includes;

use Earning_Calendar\Admin\Earning_Calendar_Client_Admin,
    Earning_Calendar\App\Controllers\Earning_calendar_Config_Controller,
    Earning_Calendar\App\Controllers\Earning_Calendar_Error_Controller,
    Earning_Calendar\Client\Earning_Calendar_Client_Public,
    Earning_Calendar\App\Controllers\Earning_Calendar_Shortcode_Controller;

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @since      1.0.0
 *
 * @package    Earning_Calendar_Client
 * @subpackage Earning_Calendar_Client/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Earning_Calendar_Client
 * @subpackage Earning_Calendar_Client/includes
 * @author     erikza@wizardsdev.com
 */
class Earning_Calendar_Client {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Earning_Calendar_Client_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $earning_calendar_client    The string used to uniquely identify this plugin.
	 */
	protected $earning_calendar_client;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'EARNING_CALENDAR_CLIENT_VERSION' ) ) {
			$this->version = EARNING_CALENDAR_CLIENT_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->earning_calendar_client = 'earning-calendar-client';

		$this->load_dependencies();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Earning_Calendar_Client_Loader. Orchestrates the hooks of the plugin.
	 * - Earning_Calendar_Client_i18n. Defines internationalization functionality.
	 * - Earning_Calendar_Client_Admin. Defines all hooks for the admin area.
	 * - Earning_Calendar_Client_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {
		$this->loader = new Earning_Calendar_Client_Loader();
		$rest_api = new Earning_Calendar_Rest_Controller($this);
        add_action('rest_api_init', [$rest_api, 'register_routes']);
        $rest_api = new Earning_Calendar_Shortcode_Controller();
        add_action('wp', [$rest_api, 'register_shortcodes']);
        add_action('wp', [$rest_api, 'register_spread_table']);
        add_action('wp', [$rest_api, 'register_selector_for_spreads']);
        add_action('wp', [$rest_api, 'register_hard_percent']);

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Earning_Calendar_Client_Admin( $this->get_earning_calendar_client(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles_and_scripts' );
        $this->loader->add_action('admin_menu', $plugin_admin, 'admin_menu');

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Earning_Calendar_Client_Public( $this->get_earning_calendar_client(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles_and_scripts' );
        add_action('earning_calendar_cron_task', [$this, 'parse_data']);

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_earning_calendar_client(): string
    {
		return $this->earning_calendar_client;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Earning_Calendar_Client_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader(): \Earning_Calendar\Includes\Earning_Calendar_Client_Loader
    {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version(): string
    {
		return $this->version;
	}

	public function parse_data($action = 'config') {
        $config = new Earning_calendar_Config_Controller($action, false);
        if (!Earning_Calendar_Error_Controller::getInstance()->is_fatal('config')) {
            $config->download_new_config();
            if ($config->is_new_data()) {
                $config->update_files();
                if (!Earning_Calendar_Error_Controller::getInstance()->is_fatal()) {
                    Earning_Calendar_Error_Controller::getInstance()->add_error(
                        'Done',
                        __FILE__ . ': ' . __LINE__,
                        'download',
                        'message'
                    );
                }
            }
        }
        return Earning_Calendar_Error_Controller::getInstance()->get_errors();
    }



}
