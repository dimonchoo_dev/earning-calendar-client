<?php

namespace Earning_Calendar\Updater;

use Earning_Calendar\App\JWT\Earning_Calendar_JWT;
use \Exception;
use \ZipArchive;

final class Earning_Calendar_Updater
{

    private $post;
    private $key;

    /**
     * @throws Exception
     */
    public function __construct($post)
    {
        $jwt = new Earning_Calendar_JWT(EARNING_CALENDAR_PRIVATE_TOKEN, 'HS256', 3600, 0);
        if (
            $_SERVER['REQUEST_METHOD'] !== 'POST' ||
            !property_exists($post, 'token') ||
            $jwt->decode($post->token)['key'] != EARNING_CALENDAR_AUTH_TOKEN ||
            !property_exists($post, 'action') ||
            defined('WPINC')
        ) {
            header($_SERVER["SERVER_PROTOCOL"] . " 410 Gone");
            echo 'Не путю';
            exit();
        }

        $this->key = ($jwt->encode(['key' => EARNING_CALENDAR_AUTH_TOKEN, 'scopes' => ['updater']]));
        $this->post = $post;
        if ($post->action === 'rollback') {
            $this->doRollback();
        } else if ('update') {
            $this->init();
        } else {
            echo 'Не туда ты зашел, братик';
        }
    }

    public function init()
    {
        $this->doBackup();
        if (!defined('EARNING_CALENDAR_AUTH_TOKEN')) {
            echo 'Нет ключа';
            exit();
        }

        $data = $this->download_file(EARNING_CALENDAR_UPDATE_URL);
        $data = json_decode($data, true, JSON_UNESCAPED_UNICODE);
        if ($data && array_key_exists('data', $data)) {
            foreach ($data['data'] as $path => $file) {
                if (strpos($path, 'updater')) continue;
                $response = $this->download_file($file);
                if ($response) {
                    $result = $this->save_file($response, $path);
                    if (!$result) {
                        $this->resetFromBackup();
                    }
                }
            }
            $response = [
                'updater' => [
                    'message' => [
                        [
                            'message' => 'Done'
                        ]
                    ]
                ]
            ];
        } else {
            $response = [
                'updater' => [
                    'fatal' => [
                        [
                            'message' => 'Fatal Error'
                        ]
                    ]
                ]
            ];
        }
        echo json_encode($response);
    }

    private function doBackup()
    {
        if (!file_exists('backup')) {
            mkdir('backup');
        }
        $files = $this->getDirContents(EARNING_CALENDAR_PLUGIN_PATH);
        $zip = new ZipArchive;
        $archive_name = 'backup/' . date('Y-m-d') . '.zip';
        if (file_exists($archive_name)) {
            unlink($archive_name);
        }
        if ($zip->open($archive_name, ZipArchive::CREATE) === TRUE) {
            foreach ($files as $file) {
                $path = str_replace(EARNING_CALENDAR_PLUGIN_PATH, '', $file);
                $zip->addFile($file, $path);
            }
            $zip->close();
        }
        if (!file_exists($archive_name)) {
            echo "Не создался бекап";
            exit();
        }
    }

    private function getDirContents($dir, &$results = array())
    {
        $files = scandir($dir);

        foreach ($files as $key => $value) {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if (in_array($value, EARNING_CALENDAR_UPDATE_EXCLUDE)) continue;
            if (!is_dir($path)) {
                $results[] = $path;
            } else if ($value != "." && $value != "..") {
                $this->getDirContents($path, $results);
            }
        }

        return $results;
    }

    private function download_file($file)
    {
        $opts = array(
            'http' => [
                'method' => "GET",
                'header' => "token:$this->key\r\n",
            ],
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
            ],
        );
        $context = stream_context_create($opts);
        $response = get_headers($file, true, $context);

        if (!$response) {
            $this->resetFromBackup();
        }
        if (!strpos($response[0], '200')) {
            $this->resetFromBackup();
        }


        $response = file_get_contents($file, false, $context);

        if (!$response) {
            $this->resetFromBackup();
        }
        return $response;
    }

    private function save_file($file, $path_to_save): bool
    {
        $path_to_save = str_replace('git/', EARNING_CALENDAR_PLUGIN_PATH, $path_to_save);
        $array = explode('/', $path_to_save);
        $dir = str_replace(end($array), '', $path_to_save);
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
        $result = file_put_contents($path_to_save, $file, LOCK_EX);
        return (bool)$result;
    }

    public function resetFromBackup()
    {
        $response = [
            'updater' => [
                'fatal' => [
                    [
                        'message' => 'Fatal Error'
                    ]
                ]
            ]
        ];
        echo json_encode($response);
        $this->post->action = 'rollback';
        $this->doRollback();
        exit();
    }

    private function doRollback()
    {
        $files = scandir('backup');
        unset($files[array_search('.', $files, true)]);
        unset($files[array_search('..', $files, true)]);
        $zip = new ZipArchive;
        if ($zip->open('backup/' . end($files)) === TRUE) {
            $zip->extractTo(EARNING_CALENDAR_PLUGIN_PATH);
            $zip->close();
        }
    }

}