<?php

namespace Earning_Calendar\Updater;

use Earning_Calendar\App\JWT\Earning_Calendar_JWT;
use Earning_Calendar\Updater\Earning_Calendar_Updater;

require_once '../config.php';
require_once EARNING_CALENDAR_PLUGIN_PATH . 'class-autoloader.php';

$post = json_decode(file_get_contents('php://input'));
try {
    new Earning_Calendar_Updater($post);
} catch (\Exception $e) {
    var_dump($e);
    exit();
}
